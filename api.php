<?php
/**
 * PHP-CRUD-API loader based on PHP version
 *
 * @package REST API plugin
 */

if ( version_compare( PHP_VERSION, '8.3', '<' ) )
{
    require_once 'api-php70-psql91-mysql56.php';
}
else
{
    // Fix PHP 8.3 deprecated errors.
    // Assumes that you also run PostgreSQL 9.5+ or MySQL 5.7+ if you run PHP8.3+
    require_once 'api-php72-psql95-mysql57.php';
}
