��          T      �       �      �      �      �   �   �      u  
   ~  1  �     �     �     �  �   �     �     �                                        API URL Authentication URL Example client Please define the <code>ROSARIO_REST_API_SECRET</code> constant in the config.inc.php file. Check installation instructions for more information. REST API User Token Project-Id-Version: REST API plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:21+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-Bookmarks: -1,-1,3,-1,-1,-1,-1,-1,-1,-1
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 URL de la API URL de autentificación Cliente ejemplo Por favor define la constante <code>ROSARIO_REST_API_SECRET</code> en el archivo config.inc.php file. Revise las instrucciones de instalación para más información. API REST Token usuario 